/*
* $Id: $
*
* Copyright (c) 2017, CGI. All rights reserved.
* CGI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/
package eu.badweather.nemtools;

import org.nem.core.crypto.KeyPair;
import org.nem.core.model.*;

/**
 * The {@code FindMyAddress} class implements ... 
 * TODO write proper class description
 * 
 * @author wiedermanna
 */
public class FindMyAddress {

   private static int threads;
   
	/**
	 * 
	 */
	public FindMyAddress() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args){
	   String prefix;
	   String suffix = null;
	   // check first 2 mandatory parameters
		if (args.length >= 2) {
		   threads = Integer.valueOf(args[0]);
		   prefix = args[1].toUpperCase();
		} else {
         throw new IllegalArgumentException("<threads> <start> <end?>");
      }
		// third optional parameter
		if (args.length == 3) {
		   suffix = args[2].toUpperCase();
		}
		for (int i=0; i<threads; i++) {
			new Thread(new FindAddress(prefix, suffix), "nemlook-"+i).start();
		}
	}

	/**
	 * 
	 * The {@code FindAddress} class implements lookup for account with given address
	 * 
	 * @author wiedermanna
	 */
	private static class FindAddress implements Runnable {
      private final String prefix;
      private final String suffix;
		
		/**
		 * @param start
		 */
		public FindAddress(String prefix, String suffix) {
			super();
         this.prefix = prefix;
         this.suffix = suffix;
		}


		@Override
		public void run() {
		   long start = System.currentTimeMillis();
			long counter = 0;
			// variant without suffix
			if (suffix==null) {
	         while (true) {
	            KeyPair kp = new KeyPair();
	            Address addr = Address.fromPublicKey(NetworkInfos.getMainNetworkInfo().getVersion(), kp.getPublicKey());
	            if (addr.getEncoded().startsWith(prefix)) {
	               found(kp, addr, counter, start);
	            }
	            counter++;
	            if (counter % 10000 == 0) {
	               System.out.println(Thread.currentThread().getName() +" tried: "+counter);
	            }
	         }
			} else {
			   // variant with suffix
	         while (true) {
	            KeyPair kp = new KeyPair();
	            Address addr = Address.fromPublicKey(NetworkInfos.getMainNetworkInfo().getVersion(), kp.getPublicKey());
	            String encodedAddr = addr.getEncoded();
	            if (encodedAddr.startsWith(prefix) && encodedAddr.endsWith(suffix)) {
	               found(kp, addr, counter, start);
	            }
	            counter++;
	            if (counter % 10000 == 0) {
	               System.out.println(Thread.currentThread().getName() +" tried: "+counter);
	            }
	         }
			}
		}
		
		private void found(KeyPair kp, Address addr, long counter, long start) {
         long allAttempts = FindMyAddress.threads * counter;
         long duration = (System.currentTimeMillis()-start)/1000;
         long throughput = FindMyAddress.threads * counter * 1000 / (System.currentTimeMillis()-start);
         System.out.println("Found it! Duration: "+duration+"s, attempts: "+allAttempts+ ", throughput: "+ throughput+" attempts/s");
         System.out.println(addr.getEncoded() + " .. "+kp.getPrivateKey() + " .. "+kp.getPublicKey());
         System.exit(0);
		}
	}
}
